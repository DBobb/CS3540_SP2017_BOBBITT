/*
 * GAPruner.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef GAPRUNER_H_
#define GAPRUNER_H_

#include <vector>
#include <map>
#include <algorithm>

#include "Pruner.h"
#include "Chromosome.h"

class GAPruner : public Pruner {
	public:
		GAPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul=false);
		GAPruner(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
					double p, bool ul=false);
		virtual ~GAPruner();

		void Init(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
					double p, bool ul=false);
		void Prune(MTRand& mt);

		void clearVectors();
		void clearTimes();
		void Reset(int np, int nt);

		void setMutationRate(double mr);
		void setCrossOverRate(double cr);


	protected:
		static bool sortVector(const std::vector<double>& elem1, const std::vector<double>& elem2);
		static bool sortPop(const Chromosome& elem1, const Chromosome elem2);

		bool isConverged();

		void initPopulation(MTRand& mt);
		void evaluateFitness();
		void updatePositions(MTRand& mt);
		void selectionAndCrossover(MTRand& mt);
		void mutate(MTRand& mt);

		//EVALUATOR EvaluateSolution;

		double pMut, pCrossover;
		double 	initTime,  fitnessTime, selectTime, crossTime, mutTime;
		double bestFitness, totalFitness;

		std::vector <Chromosome> pop;
};

#endif /* GAPRUNER_H_ */
